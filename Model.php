<?php
abstract class Model{
  //Declaramos variables globales
  protected $user;
  protected $pass;
  protected $db;
  protected $host;
  protected $port;
  protected $conn;
  protected static $all;
  protected $table;
  //Constructor donde asignamos el valor correspondiente a cada variable
  function __construct(){
    $this->user="root";
    $this->pass="";
    $this->db="lobo";
    $this->host="localhost";
    $this->port=3306;
  }
  //Funcion donde traemos todas las filas de la tabla
  public function all(){
    return $this->executeQuery("select * from ".$this->table);
  }
  public function add($columns,$values){
     $this->openConnection();
    return $this->conn->query("INSERT INTO ".$this->table." (".$columns.") VALUES(".$values.")");
    $this->closeConection();
  }
  public function delete($value){
     $this->openConnection();
    return $this->conn->query("DELETE FROM ".$this->table." WHERE '$value'");
    $this->closeConection();
  }
  public function edit($values,$where){
     $this->openConnection();
    return $this->conn->query("UPDATE ".$this->table." SET ".$values." WHERE ".$where."");
    $this->closeConection();
  }

  //Funcion donde traemos las filas con el campo que corresponde al valor dado
  public function where($field,$value){
    return $this->executeQuery("select * from ".$this->table." where {$field} = {$value}");
  }
  //Funcion donde ejecutamos la sentencia SQL formada por otra funcion
  public function executeQuery($query){
    //Se abre la conexion;
    $this->openConnection();
    //Se obtiene el resultset de la consulta
    $result = $this->conn->query($query);
    //Obtenenos todas las filas y las ponemos en data
    $data = [];
    while($row = $result->fetch_assoc()){ array_push($data,$row); }
    //Cerramos la conexion
    $this->closeConection();
    //Regresamos los datos
    return $data;
  }
  //Abrimos la conexion
  public function openConnection(){
    $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db,$this->port);
    $this->conn->set_charset('utf8');
  }
  //Cerramos la conexion
  public function closeConection(){
    mysqli_close($this->conn);
  }
}
